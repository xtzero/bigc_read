<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>大白猫的已读</title>
    <style lang="css">
        body {
            margin: 0;
        }
        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            width: 100vw;
            height: 100vh;
            color: #000;
        }
        .content {
            border: dashed #000 1px;
            padding: 100px 200px;
            font-size: 2em;
        }
        .tip, .tip>a {
            margin-top: 100px;
            font-size: 0.8em;
            color: gray;
        }
        @media screen and (max-width: 500px) {
            .content {
                padding: 10vw 20vw;
                font-size: 30px;
            }
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">{{ $link['origin'] }}</div>
    <div class="tip">来自 <a href="/">大白猫的已读</a></div>
</div>
</body>
</html>
