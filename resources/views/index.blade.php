<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>大白猫的已读</title>
    <style lang="css">
        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 100vw;
            height: 100vh;
        }
        .logo {
            width: 200px;
            height: 200px;
            margin-top: 10vh;
            border-radius: 50%;
        }
        .title {
            font-size: 3em;
            margin: 20px 0;
            font-weight: 500;
        }
        .kwd {
            border: solid lightgray 1px;
            border-radius: 5px;
            width: 50vw;
            height: 5vh;
            padding: 0.5vw;
            font-size: 1.5em;
            text-align: center;
        }
        .commit {
            width: 10vw;
            height: 5vh;
            background-color: #000;
            color: #fff;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            font-size: 1.2em;
            border-radius: 10px;
            margin-top: 10vh;
        }
        .commit:hover {
            cursor: pointer;
            background-color: #148AF0;
        }
        .commit:active {
            background-color: #366065;
        }
        .res {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-top: 5vh;
            border: dashed 1px gray;
            padding: 10px 20px;
        }
        .res-title {
            font-size: 2.5em;
            font-weight: 500;
        }
        .res-url {
            text-decoration: underline;
            font-size: 1.5em;
        }
        .res-url:hover {
            color: gray;
            cursor: pointer;
        }
        .res-url:active {
            color: lightgray;
        }
        .res-tip {
            font-size: 0.8em;
            color: gray;
            margin-top: 10px;
        }
        @media screen and (max-width: 500px) {
            .logo {
                margin-top: 10vh;
            }
            .title {
                font-size: 30px;
            }
            .kwd {
                width: 80vw;
                font-size: 13px;
            }
            .commit {
                margin-top: 20px;
                font-size: 15px;
                width: 50vw;
            }
            .res {
                width: 70vw;
                margin-top: 10vh;
            }
            .res-title {
                font-size: 1em;
                font-weight: 500;
            }
            .res-url {
                text-decoration: underline;
                font-size: 15px;
            }
            .res-url:hover {
                color: gray;
                cursor: pointer;
            }
            .res-url:active {
                color: lightgray;
            }
            .res-tip {
                font-size: 0.8em;
                color: gray;
                margin-top: 10px;
            }
        }
    </style>
    <script src="{{\Illuminate\Support\Facades\URL::asset('//code.jquery.com/jquery-3.6.0.min.js')}}"></script>
</head>
<body>
    <div class="container">
        <img src="/logo.png" alt="" class="logo">
        <div class="title">大白猫的已读</div>
        <input type="text" placeholder="不管你讲什么，大白猫都会「已读」" class="kwd">
        <div class="res-tip">其实是个短链接生成器啦</div>
        <div class="commit">你闻闻</div>

        <div class="res">
            <div class="res-title">喵~</div>
            <div class="res-url">
                <a href="" class="res-url-a" target="_blank">...</a>
            </div>
{{--            <div class="res-tip">点击链接复制</div>--}}
        </div>
    </div>
    <script>
        (function() {
            $('.res').hide()
            $('.kwd').val('')

            $('.commit').on('click', function() {
                if ($(this).text() === '喵… （请稍等）') {
                    return
                }

                var kwd = $('.kwd').val()
                if (!kwd) {
                    alert("你倒是让大白猫读点什么啊 \n (╯‵□′)╯︵┻━┻")
                    return
                }

                $(this).text('喵… （请稍等）')
                $.ajax({
                    url: `/handle?url=${kwd}`,
                    success(res) {
                        if (![200, 201].includes(res.code)) {
                            alert(res.msg)
                            return
                        }
                        $('.commit').text('你闻闻')
                        $('.res-url-a').text('{{$currUrl}}' + res.data.code)
                        $('.res-url-a').attr('href', '{{$currUrl}}' + res.data.code)
                        $('.res').show()
                    },
                    fail() {
                        $('.commit').text('你闻闻')
                        alert("喵呜呜呜呜……\n(「程序出了点问题」的意思)")
                    }
                })
            })
        })()
    </script>
</body>
</html>
