<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table = function (Blueprint $table) {
            !Schema::hasColumn('link', 'id') ? $table->id() : '';
            !Schema::hasColumn('link', 'code') ? $table->string('code') : '';
            !Schema::hasColumn('link', 'origin') ? $table->string('origin') : '';
            !Schema::hasColumn('link', 'create_time') ? $table->string('create_time') : '';
            !Schema::hasColumn('link', 'create_ip') ? $table->string('create_ip') : '';
        };

        Schema::hasTable('link') ?
            Schema::table('link', $table) :
            Schema::create('link', $table);
}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link');
    }
}
