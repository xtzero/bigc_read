<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $table = 'link';

    protected $attributes = [
        'code' => '',
        'origin' => '',
        'create_time' => '',
        'create_ip' => ''
    ];

    public $timestamps = false;
}
