<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\IndexController;

Route::get('/', [IndexController::class, 'index']);
Route::get('/handle', [IndexController::class, 'handle']);
Route::get('/{code}', [IndexController::class, 'visit']);
